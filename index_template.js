const axios = require('axios');
const puppeteer = require('puppeteer');

const apiKey = "dbba0b1d889d3d0a055ed005d709d800160d2680";
const testId = "5ba1c33ed672630e2118f479";

async function run() {

    const j = (await axios.get(`https://api.ghostinspector.com/v1/tests/${testId}/?apiKey=${apiKey}`)).data;
    // console.log(j);
    const steps = j.data.steps;
    console.log("xxx step[0]: ", steps[0]);
    const browser = await puppeteer.launch({
        headless: false
    });
    const page = await browser.newPage();

    await page.goto('https://qa.take247.co.il');
    for (let i = 0; i < steps.length; i++) {
        const step = steps[i];
        console.log(`STEP ${i}: ${step.command} ${step.target} ${step.value}`);
        switch (step.command) {
            case "eval":
                page.evaluate(step.value);
                break;
            case "pause":
                await new Promise((resolve, reject) => setTimeout(resolve, step.value));
                break;
            case "assertElementVisible":
                await page.waitForSelector(step.target, {
                    visible: true,
                });
                break;
            case "click":
                {
                    const elements = await page.$x(`//*[@id='${step.target.replace('#','')}']`);
                    //console.log("xxx1", elements);
                    await elements[0].click();
                    break;
                }
            case "assign":
                {
                    const elements = await page.$x(`//*[@id='${step.target.replace('#','')}']`);
                    //console.log("xxx2", elements);
                    await elements[0].click();
                    await page.type(step.target, step.value);
                    break;
                }
        }

    }
    //await page.screenshot({ path: './take247.png' });

    //browser.close();
}

run();