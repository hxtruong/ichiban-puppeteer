const testCase = require("./testCase.js")
const puppeteer = require("puppeteer");
const axios = require("axios")
const chalk = require("chalk")

// const apiKey = "dbba0b1d889d3d0a055ed005d709d800160d2680";
const apiKey = "dbba0b1d889d3d0a055ed005d709d800160d2680";
const suiteId = "5b8fafeef3cd28411d01436a"
const testId = "5ba1c33ed672630e2118f479";
const CUSTOM_TEST = true;

const screenshotPath = "./screenshot"
// const urlWeb = 'http://localhost:3000'
//const urlWeb = 'https://master.take247.co.il'
//const urlWeb = 'https://qa.take247.co.il'
const urlWeb = 'http://localhost:8037'
const customTestListId = [
    //"5ba1c33ed672630e2118f46f",
    //"5ba1c33ed672630e2118f484",
    //"5ba1c33ed672630e2118f483",
    //"5ba1c33ed672630e2118f454",
    "5ba1c33ed672630e2118f470"
]

async function getListSuiteTestId(suiteId, apiKey) {
    //GEThttps://api.ghostinspector.com/v1/suites/<suiteId>/tests/?apiKey=<apiKey>
    const testList = (await axios.get(`https://api.ghostinspector.com/v1/suites/${suiteId}/tests/?apiKey=${apiKey}`)).data.data;
    // console.log(testList)
    //const testListId = testList.map(test => {
    //    console.log(test.name)
    //    return test._id
    //})
    //return testListId;
    return testList;
}

runTestListId = async (browser, testListId) => {
    let numberTestPassed = 0;
    let failedTestListId = []
    let count = 0;
    for (let index in testListId) {
        if (CUSTOM_TEST) {
            count++;
            if (count == 2) break;
        }
        let testId = testListId[index]
        console.log(testId)
        const page = await browser.newPage();
        await page.goto(urlWeb, {
            timeout: 60000,
        });
        // describe('Google', async () => {
        const isPassed = await testCase(page, apiKey, testId, `${screenshotPath}/${testId}.png`);

        isPassed ? numberTestPassed++ : failedTestListId.push(testId);
        // })

        //browser.close();
    }

    console.log(chalk.red("List of failed test Id:"))
    failedTestListId.every(testId => console.log(testId))
    return numberTestPassed;
}

async function runListTestCase() {
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 25,
    });
    let testList = []
    if (CUSTOM_TEST) testList = await getListSuiteTestId(suiteId, apiKey)
    // for (const key in testListId) {
    //     if (testListId.hasOwnProperty(key)) {
    //         const id = testListId[key];
    //         console.log(`Number test ${key} with ID=${id}`)
    //     }
    // }
    let numberTestPassed = 0;
    if (CUSTOM_TEST) {
        numberTestPassed = await runTestListId(browser, customTestListId);
    } else {
        const testListId = testList.map(test => test._id)
        numberTestPassed = await runListTestCase(browser, testListId)
    }

    console.log("===================================================")
    console.log(`${chalk.green(numberTestPassed)} test PASSED!`)
    console.log(`${chalk.red(CUSTOM_TEST?customTestListId.length:testList.length-numberTestPassed)} test FAILED`)
}

runListTestCase()