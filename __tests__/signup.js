require('dotenv').config()
const faker = require('faker')

const account = {
  userName: faker.internet.email(),
  password: faker.internet.password(),
}

describe(
  '/ (Home Page)',
  () => {
    let page
    beforeAll(async () => {
      page = await global.__BROWSER__.newPage()
      await page.goto(`${process.env.ICHIBAN_URL}/login`)
    }, process.env.TIMEOUT)

    test('Sign up Ichiban', async () => {
      const sigupDlg = await page.$x('//*[@id="signup"]');
      await sigupDlg[0].click();

      const username = await page.$x('//*[@id="username"]');
      await username[0].type(account.userName);

      const pass = await page.$x('//*[@id="password"]');
      await pass[0].type(account.password);

      const signup_btn = await page.$x('//*[@id="signup-btn"]');
      await signup_btn[0].click();

      expect(await page.$x('//*[@id="signed-flag-true"]')).toBeTruthy()

    })
  },
  process.env.TIMEOUT
)