require('dotenv').config()

describe(
  '/ (Home Page)',
  () => {
    let page
    beforeAll(async () => {
      page = await global.__BROWSER__.newPage()
      await page.goto(process.env.ICHIBAN_URL)
    }, process.env.TIMEOUT)

    test('login to Ichiban', async () => {
      const loginDlg = await page.$x('//*[@id="login"]');
      await loginDlg[0].click();

      const username = await page.$x('//*[@id="username"]');
      await username[0].type(process.env.USER_NAME);

      const pass = await page.$x('//*[@id="password"]');
      await pass[0].type(process.env.PASSWORD);

      const login_btn = await page.$x('//*[@id="login-btn"]');
      await login_btn[0].click();

      await page.waitForNavigation();
      expect(page.url()).toEqual(`${process.env.ICHIBAN_URL}/dashboard`);

    })
  },
  process.env.TIMEOUT
)