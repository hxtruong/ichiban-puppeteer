require('dotenv').config()
const axios = require('axios');
const faker = require('faker')
const chalk = require('chalk')
var cssToXPath = require('css-to-xpath');

processStringValue = (value) => {
    if (value.indexOf("{{TIMESTAMP}}") > -1) {
        return value.replace("{{TIMESTAMP}}", faker.finance.account())
    } else if (value.indexOf("{{ALPHANUMERIC}}") > -1) {
        return value.replace("{{ALPHANUMERIC}}", faker.commerce.productName())
    }
    return value

}
getElementsPage = async (page, target) => {
    console.log("XPath: ", chalk.yellow(cssToXPath(target)))
    let elements = []

    // console.log(chalk.blue("Target: ", target))
    if (target.indexOf("xpath") > -1) { // XPath
        elements = await page.$x(`${target.substring(6)}`); // start after "xpath="
    } else {
        elements = await page.$x(cssToXPath(target))
    }

    if (elements && elements.length > 0)
        return elements
    else {
        throw new Error(chalk.red(`Link not found: ${target}`));
    }
}

getKeyCode = (key) => {
    switch (key) {
        case 'down':
            return "ArrowDown"
        case 'up':
            return "ArrowUp"
        case 'right':
            return "ArrowRight"
        case 'left':
            return "ArrowLeft"
        case '13':
            return "Enter"
        default:
            return key;
    }
}

const main = async function run(page, apiKey, testId, screenshotPath) {
    const testCase = (await axios.get(`https://api.ghostinspector.com/v1/tests/${testId}/?apiKey=${apiKey}`)).data;
    console.log(chalk.yellow("TEST CASE: ", chalk.blue(testCase.data.name)));
    let isSuccessful = true;
    const steps = testCase.data.steps;
    screenshotPath ? console.log("Length step: ", steps.length) : null;
    for (let i = 0; i < steps.length; i++) {
        const step = steps[i];
        console.log(`${chalk.magenta("STEP")} ${chalk.cyan(i)}: ${chalk.blue(step.command)} ${chalk.yellow(step.target)} ${chalk.green(step.value)}`);
        switch (step.command) {
            case "execute":
                {
                    console.log(chalk.green("Start execute another test: ", step.value))
                    isSuccessful = await run(page, apiKey, step.value);
                    console.log(chalk.green("Finished executing."))
                    break;
                }
            case "eval":
                {
                    try {
                        await page.evaluate(step.value);
                    } catch (error) {
                        isSuccessful = false;
                        console.log(chalk.red("Failed: ", error))
                    }
                    break;
                }
            case "pause":
                {
                    await new Promise((resolve, reject) => setTimeout(resolve, step.value / 2));
                    break;
                }
            case "assertElementVisible":
                {
                    try {
                        await page.waitForSelector(step.target, {
                            visible: true,
                        });
                        console.log(`${step.target} is visible`)
                    } catch (error) {
                        isSuccessful = false;
                        console.log(chalk.red("Failed: ", error))
                    }

                    break;
                }
            case "click":
                {

                    let elements = await getElementsPage(page, step.target);
                    try {
                        await elements[0].click()
                    } catch (error) {
                        isSuccessful = false;
                        console.log(chalk.red("Failed: ", error))
                    }

                    await new Promise((resolve, reject) => setTimeout(resolve, 1000));
                    break;
                }
            case "assign":
                {
                    step.value = processStringValue(step.value);
                    console.log("Value: ", step.value);
                    try {
                        if (step.value === "{{username}}")
                            await page.type(step.target, process.env.USER_NAME);
                        else if (step.value === "{{password}}")
                            await page.type(step.target, process.env.PASSWORD);
                        else
                            await page.type(step.target, step.value);
                    } catch (error) {
                        isSuccessful = false;
                        console.log(chalk.red("Failed: ", error))
                    }

                    break;
                }
            case "assertElementPresent":
                {
                    try {
                        await page.waitForSelector(step.target);
                    } catch (error) {
                        isSuccessful = false;
                        console.log(chalk.red("Failed: ", error))
                    }
                    break;
                }
            case "assertTextPresent":
                {
                    let elements = await getElementsPage(page, step.target);
                    const text = await (await elements[0].getProperty(`textContent`)).jsonValue();
                    console.log("Assert Text Present: ", text == step.value ? chalk.green("TRUE") : chalk.red("FALSE"))
                    if (text != step.value) isSuccessful = false;
                    break;
                }
            case "screenshot":
                {
                    if (screenshotPath) {
                        await page.screenshot({
                            path: `./screenshot/${testId+'-'+i}.png`
                        })
                    }
                    break;
                }
            case "keypress":
                {
                    // console.log("Detail: ", step, " | Type: ", typeof (step.value))
                    let keyCode = getKeyCode(step.value);
                    console.log("Press: ", chalk.green(keyCode + "\nType:", typeof (keyCode)))
                    try {
                        await page.keyboard.press(keyCode, {
                            delay: 250
                        });
                    } catch (error) {
                        isSuccessful = false;
                        console.log(chalk.red("Failed: ", error))
                    }
                    break;
                }
            case "assertEval":
                {
                    try {
                        // console.log(await page.evaluate('1 + 2')); // prints "3"
                        // console.log(typeof (step.value))
                        // TODO: many return in a function
                        let script = step.value.replace("return", "")
                        const result = await page.evaluate(script);
                        console.log("Javascript return True: ", result ? chalk.green(result) : chalk.red(result))
                        if (!result) isSuccessful = false;
                    } catch (error) {
                        console.log(chalk.red("Failed: ", error))
                    }
                    break;
                }
            case "assertText":
                {
                    //TODO: What different 'assert Text and assert Text present
                    let elements = await getElementsPage(page, step.target);
                    const text = await (await elements[0].getProperty(`textContent`)).jsonValue();
                    console.log("Assert Text: ", text == step.value ? chalk.green("TRUE") : chalk.red("FALSE"))
                    if (text != step.value) isSuccessful = false;

                    break;
                }
            case "mouse over":
                {
                    console.log(`${chalk.yellow("TODO:")} Implement`)
                    break;
                }
            case "drag and drop":
                {
                    console.log(`${chalk.yellow("TODO:")} Implement`)
                    break;
                }
            case "gotoURL":
                {
                    console.log(`${chalk.yellow("TODO:")} Implement`)
                    break;
                }
            case "refresh":
                {
                    await page.keyboard.press("F5", {
                        delay: 1000
                    });
                    break;
                }
            case "assertElementNotPresent":
                {
                    try {
                        const selector = await page.waitForSelector(step.target, {
                            hidden: true
                        });
                    } catch (error) {
                        isSuccessful = false;
                        console.log(chalk.red("Failed: ", error))
                    }
                    break;
                }
            case "assertElementNotVisible":
                {
                    try {
                        await page.waitForSelector(step.target, {
                            visible: false,
                        });
                    } catch (error) {
                        isSuccessful = false;
                        console.log(chalk.red("Failed: ", error))
                    }
                    break;
                }
            default:
                {
                    isSuccessful = false;
                    console.log(chalk.red("NO EXIST THIS COMMAND"))
                    return
                }
        }

    }

    // only run when it is main test case. it is not runned from another test
    // case
    if (screenshotPath) {
        await page.screenshot({
            path: screenshotPath
        });
        console.log(`${chalk.green("FINISHED")}`)
    }
    if (!isSuccessful) {
        console.log(`${chalk.red("This test is not pass completelly!!!")}`)
    }
    return isSuccessful;

}

module.exports = main